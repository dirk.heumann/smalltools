# smallTools

These are just some very simple tools that ease the handling of the command line and of git

You can copy and use it freely.

Extreme simple everyday scripts are in bin for git for Windows, Unix (partly tk required)

script           | Description
---------------- |------------
bin/xt           | starts graphical terminal
bin/gg           | starts "git gui" detached, without blocking
bin/gk           | starts "gitk" detached, without blocking
bin/gka          | starts "gitk --all" detached, without blocking
bin/gitPrompt.sh | git prompt, which can be sourced, accepts bw / color as parameter

You may put these in your local ~/bin folder
